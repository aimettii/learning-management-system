export default function({ $auth, error, store, route, redirect}) {
    const authenticated = $auth.loggedIn;
    
    if (! authenticated) {
        return $auth.redirect('login')
    }
    
    if ($auth.user.role !== 'teacher') {
        
        error({ message: "Access is denied" });
    }
}