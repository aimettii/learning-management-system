export default function(context) {
    context.route.meta.forEach((meta => {
        if (meta.redirect) {
            if (meta.redirect[0] === context.route.path) {
                meta.redirect[1](context);
            }
        }
    }))
}