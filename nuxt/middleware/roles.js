export default function({$auth, route, error}) {
    let roles = [];
    
    const authenticated = $auth.loggedIn;
    
    if (! authenticated) {
        return $auth.redirect('login')
    }
    
    route.meta.forEach((meta => {
        if (meta.roles) {
            roles = Array.isArray(meta.roles) ? meta.roles : [meta.roles]
            return false;
        }
    }))
    
    if (! roles.length) {
        throw new Error('It is necessary to set the correct roles meta property')
    }
    
    if (! roles.includes($auth.user.role)) {
        error({ message: "Access is denied" });
    }
}