export default function({ $auth, error, store, route, redirect}) {
    if ($auth.getState('_unauthenticated')) {
        $auth.$storage.setState('_unauthenticated', false)
        setTimeout(() => {
            $auth.setUser(null);
        }, 1)
    } else {
        const authenticated = $auth.loggedIn;

        if (authenticated) {
            return $auth.redirect('home')
        }
    }
}