const webpack = require('webpack')

const i18nConfig = require('./config/i18n')
const defaultLocale = i18nConfig.defaultLocale
const fallbackLocale = i18nConfig.fallbackLocale
const dateTimeFormats = i18nConfig.dateTimeFormats
const numberFormats = i18nConfig.numberFormats
const locales = i18nConfig.locales

module.exports = {
    ssr: false,
    target: 'static',
    
    /*
    ** Headers of the page
    */
    head () {
        return {
            title: 'Codeaily',
            htmlAttrs: {
                dir: `${this.$store && this.$store.getters.settings.layout.rtl
                  ? 'rtl'
                  : 'ltr'}`,
            },
            meta: [
                { charset: 'utf-8' },
                {
                    name: 'viewport',
                    content: 'width=device-width, initial-scale=1',
                },
                {
                    hid: 'description',
                    name: 'description',
                    content: `Codeaily Learning Management System`,
                },
                { hid: 'robots', name: 'robots', content: 'noindex' },
            ],
            link: [
                { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
                {
                    href: 'https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&amp;display=swap',
                    rel: 'stylesheet',
                },
                {
                    href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css',
                    rel: 'stylesheet',
                },
                  {
                        href: 'https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.css',
                        rel: 'stylesheet',
                    },
                  {
                        href: 'https://uicdn.toast.com/tui-color-picker/latest/tui-color-picker.css',
                        rel: 'stylesheet',
                    },
                  
            ],
            script: [
                // { src: 'https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.6.7/fabric.min.js'},
                // { src: 'https://cdn.jsdelivr.net/npm/tui-code-snippet@1.4.0/dist/tui-code-snippet.min.js'},
                // { src: 'https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.min.js'}
                { src: 'https://js.stripe.com/v3/'}
            ]
            
        }
    },
    
    server: {
        host: process.env.FRONTEND_URL || 'frontend.lesson.local', // default: localhost
        port: process.env.FRONTEND_PORT || 80,
    },
    
    /*
    ** Customize the progress-bar color
    */
    loading: {
        color: '#5567FF',
        height: '3px',
        continuous: true,
        throttle: 0,
    },
    
    /*
    ** Global CSS
    */
    css: [
        '~/assets/scss/vendor/material-icons.scss',
        '~/assets/scss/vendor/fontawesome.scss',
        '~/assets/scss/app.scss',
        '~/assets/scss/vendor/quill.scss',
    ],
    
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '~/plugins/app',
        '~/plugins/vue-luma',
        '~/plugins/fmv-avatar',
        '~/plugins/fmv-layout',
        '~/plugins/fmv-charts',
        '~/plugins/fmv-highlight',
        '~/plugins/fmv-input-group-merge',
        '~/plugins/bv-form-image-group',
        '~/plugins/vue-quill-editor.client',
        '~/plugins/vue-api-query',
        '~/plugins/axios',
        
        // Settings client middleware (handles dynamic layout route settings
        // client side)
        '~/plugins/settings.client',
        '~/plugins/froala-editor',
        '~/plugins/before-each',
    ],
    
    // Modules for dev and build (recommended)
    // (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // // https://go.nuxtjs.dev/eslint
        // '@nuxtjs/eslint-module',
    ],
    
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        '@nuxtjs/axios',
        '@nuxtjs/auth-next',
        'vue-sweetalert2/nuxt',
        // Doc: https://bootstrap-vue.js.org/docs/
        ['bootstrap-vue/nuxt', { css: false }],
        // Doc: https://nuxt-community.github.io/nuxt-i18n/
        [
            'nuxt-i18n', {
            lazy: true,
            vueI18nLoader: true,
            langDir: 'locales/',
            locales,
            defaultLocale,
            vueI18n: {
                fallbackLocale,
                dateTimeFormats,
                numberFormats,
                silentTranslationWarn: true,
            },
        }],
        'nuxt-svg-loader',
    ],
    
    /*
    ** Axios module configuration
    */
    axios: {
        baseURL: process.env.API_URL,
        withCredentials: true,
        timeout: 1000
    },
    
    publicRuntimeConfig: {
        axios: {
            baseURL: process.env.API_URL,
            withCredentials: true,
            timeout: 10
        },
    },
    
    auth: {
        watchLoggedIn: false,
        autoFetch: true,
        redirect: {
            login: '/auth/login',
            // logout: '/',
            callback: '/auth/login',
            // home: '/',
        },
        strategies: {
            local: false,
            cookie: {
                cookie: {
                    // (optional) If set we check this cookie existence for loggedIn check
                    name: 'XSRF-TOKEN',
                },
                user: {
                    autoFetch: true,
                    property: 'data',
                },
                endpoints: {
                    csrf: {
                        url: '/sanctum/csrf-cookie',
                        withCredentials: true,
                    },
                    login: {
                        url: '/auth/login',
                        method: 'post',
                        withCredentials: true,
                        timeout: 10000,
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                            'Content-Type': 'application/json',
                        },
                    },
                    logout: {
                        url: '/auth/logout',
                        method: 'post',
                        withCredentials: true,
                        timeout: 10000,
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                            'Content-Type': 'application/json',
                        },
                    },
                    user: {
                        url: '/auth/account',
                        method: 'get',
                        withCredentials: true,
                        autoFetch: true,
                        timeout: 10000,
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                            'Content-Type': 'application/json',
                        },
                    },
                },
                tokenRequired: false,
                tokenType: false,
               
            },
        },
    },
    
    render: {
        resourceHints: false,
    },
    
    router: {
        base: '/',
        linkActiveClass: 'active',
        linkExactActiveClass: 'active',
        middleware: [
            // DEMO: handles dynamic layout route settings server side
            'settings',
        ],
    },
    
    generate: {
        // routes (callback) {
        //     const routes = [
        //         '/home',
        //     ]
        //     callback(null, routes)
        // },
	      concurrency: 10,
        crawler: false
    },
    
    hooks: {
        'build:done'() {
            const modulesToClear = ['vue', 'vue/dist/vue.runtime.common.prod']
            modulesToClear.forEach((entry) => {
                delete require.cache[require.resolve(entry)]
            })
        },
    },
    
    /*
    ** Build configuration
    */
    build: {
        maxChunkSize: 300000,
        
        extractCSS: true,
        
        // postcss: {
        //     plugins: {
        //         'postcss-rtl': {},
        //     },
        // },
    
        html: {
            minify: {
                collapseBooleanAttributes: true,
                decodeEntities: true,
                minifyCSS: false,
                minifyJS: false,
                processConditionalComments: true,
                removeEmptyAttributes: true,
                removeRedundantAttributes: true,
                trimCustomFragments: true,
                useShortDoctype: true
            }
        },
        
        babel: {
            sourceType: 'unambiguous',
            plugins: [
                '@babel/plugin-syntax-export-default-from',
                [
                    '@babel/plugin-transform-runtime', {
                    'helpers': true,
                    'forceAllTransforms': false,
                }],
            ],
            presets: [
                [
                    '@babel/preset-env', {
                    'debug': false,
                    'useBuiltIns': 'usage',
                    'corejs': '3.6',
                    'modules': 'auto',
                    'targets': {
                        'browsers': [
                            'last 2 versions',
                            'ie >= 11',
                        ],
                    },
                }],
            ],
            compact: true,
        },
        
        loaders: {
            scss: {
                implementation: require('sass'),
                sassOptions: {
                    includePaths: ['node_modules'],
                },
            },
        },
        
        plugins: [
            new webpack.ProvidePlugin({
                '$': 'jquery', // used by ui-icons page demo
                'domFactory': 'dom-factory',
            }),
        ],
        
        extend (config, { isDev, isClient }) {
            config.resolve.alias['vue'] = 'vue/dist/vue.common'
            
            // do not resolve symlinks
            if (isDev) {
                config.resolve.symlinks = false
            }
        },
    },
}
