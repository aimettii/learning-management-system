function posY(elm) {
    var test = elm, top = 0;
    
    while(!!test && test.tagName.toLowerCase() !== "body") {
        top += test.offsetTop;
        test = test.offsetParent;
    }
    
    return top;
}

function viewPortHeight() {
    var de = document.documentElement;
    
    if(!!window.innerWidth)
    { return window.innerHeight; }
    else if( de && !isNaN(de.clientHeight) )
    { return de.clientHeight; }
    
    return 0;
}

function scrollY() {
    if( window.pageYOffset ) { return window.pageYOffset; }
    return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
}

function checkVisible(elm, threshold, mode) {
    if (!elm) return false;
    threshold = threshold || 0;
    mode = mode || 'visible';
    
    var rect = elm.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    var above = rect.bottom - threshold < 0;
    var below = rect.top - viewHeight + threshold >= 0;
    
    return mode === 'above' ? above : (mode === 'below' ? below : !above && !below);
}

export default checkVisible;