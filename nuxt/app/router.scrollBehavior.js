export default function (to, from, savedPosition) {
  if (to.hash === '#') {
      history.replaceState("", document.title, window.location.pathname + window.location.search);
    return null;
  }else if(to.hash) {
      history.replaceState("", document.title, window.location.pathname + window.location.search);
      return { selector: to.hash }
  }
  if (savedPosition) {
    return savedPosition
  }
  return { x: 0, y: 0 }
}