export const grades = (fromToArr = null) => {
    const baseGrades = [
        {
            "name": "Pre-K",
            "abbr": "Pre-K",
            "short_description": "Counting objects, inside and outside, longer and shorter, letter names, rhyming words, and more.",
            "short_skills": [
                {
                    "name": "Learn to count - up to 3",
                    "href": "https://www.ixl.com/math/pre-k/learn-to-count-up-to-3"
                },
                {
                    "name": "Count using stickers - up to 5",
                    "href": "https://www.ixl.com/math/pre-k/count-using-stickers-up-to-5"
                },
                {
                    "name": "Above and below",
                    "href": "https://www.ixl.com/math/pre-k/above-and-below"
                },
                {
                    "name": "Classify and sort by shape",
                    "href": "https://www.ixl.com/math/pre-k/classify-and-sort-by-shape"
                },
                {
                    "name": "Shape patterns",
                    "href": "https://www.ixl.com/math/pre-k/shape-patterns"
                },
                {
                    "name": "Name the shape",
                    "href": "https://www.ixl.com/math/pre-k/name-the-shape"
                },
                {
                    "name": "Pennies and nickels",
                    "href": "https://www.ixl.com/math/pre-k/pennies-and-nickels"
                }
            ]
        },
        {
            "name": "Kindergarten",
            "abbr": "K",
            "short_description": "Comparing numbers, names of shapes, consonant and vowel sounds, sight words, and more.",
            "short_skills": [
                {
                    "name": "Skip-count by tens",
                    "href": "https://www.ixl.com/math/kindergarten/skip-count-by-tens"
                },
                {
                    "name": "Squares",
                    "href": "https://www.ixl.com/math/kindergarten/squares"
                },
                {
                    "name": "Subtract - numbers up to 10",
                    "href": "https://www.ixl.com/math/kindergarten/subtract-numbers-up-to-10"
                },
                {
                    "name": "Make a number using addition - sums up to 5",
                    "href": "https://www.ixl.com/math/kindergarten/make-a-number-using-addition-sums-up-to-5"
                },
                {
                    "name": "Counting on the hundred chart",
                    "href": "https://www.ixl.com/math/kindergarten/counting-on-the-hundred-chart"
                }
            ]
        },
        {
            "name": "First grade",
            "abbr": "1",
            "short_description": "Adding and subtracting, tens and ones, telling time, categories, nouns, verb tense, time order, and more.",
            "short_skills": [
                {
                    "name": "Place value models up to 20",
                    "href": "https://www.ixl.com/math/grade-1/place-value-models-up-to-20"
                },
                {
                    "name": "Subtract multiples of 10",
                    "href": "https://www.ixl.com/math/grade-1/subtract-multiples-of-10"
                },
                {
                    "name": "Select three-dimensional shapes",
                    "href": "https://www.ixl.com/math/grade-1/select-three-dimensional-shapes"
                },
                {
                    "name": "Equal parts - halves and fourths",
                    "href": "https://www.ixl.com/math/grade-1/equal-parts-halves-fourths"
                }
            ]
        },
        {
            "name": "Second grade",
            "abbr": "2",
            "short_description": "Place-value models, contractions, irregular plurals, plants and animals, historical figures, and more.",
            "short_skills": [
                {
                    "name": "Add and subtract numbers up to 100",
                    "href": "https://www.ixl.com/math/grade-2/add-and-subtract-numbers-up-to-100"
                },
                {
                    "name": "Measure using an inch ruler",
                    "href": "https://www.ixl.com/math/grade-2/measure-using-an-inch-ruler"
                },
                {
                    "name": "Identify a digit up to the hundreds place",
                    "href": "https://www.ixl.com/math/grade-2/identify-a-digit-up-to-the-hundreds-place"
                },
                {
                    "name": "Create line plots",
                    "href": "https://www.ixl.com/math/grade-2/create-line-plots"
                },
                {
                    "name": "Number lines - up to 100",
                    "href": "https://www.ixl.com/math/grade-2/number-lines-up-to-100"
                }
            ]
        },
        {
            "name": "Third grade",
            "abbr": "3",
            "short_description": "Multiplying and dividing, bar graphs, pronouns, possessives, weather and climate, geography, and more.",
            "short_skills": [
                {
                    "name": "Multiplication tables up to 10",
                    "href": "https://www.ixl.com/math/grade-3/multiplication-tables-up-to-10"
                },
                {
                    "name": "Divide by counting equal groups",
                    "href": "https://www.ixl.com/math/grade-3/divide-by-counting-equal-groups"
                },
                {
                    "name": "Graph fractions on number lines",
                    "href": "https://www.ixl.com/math/grade-3/graph-fractions-on-number-lines"
                },
                {
                    "name": "Show fractions: fraction bars",
                    "href": "https://www.ixl.com/math/grade-3/show-fractions-fraction-bars"
                },
                {
                    "name": "Create rectangles with a given area",
                    "href": "https://www.ixl.com/math/grade-3/create-rectangles-with-a-given-area"
                }
            ]
        },
        {
            "name": "Fourth grade",
            "abbr": "4",
            "short_description": "Fractions and decimals, synonyms and antonyms, fossils and rock layers, government, and more.",
            "short_skills": [
                {
                    "name": "Decompose fractions into unit fractions",
                    "href": "https://www.ixl.com/math/grade-4/decompose-fractions-into-unit-fractions"
                },
                {
                    "name": "Multiply a 2-digit number by a larger number",
                    "href": "https://www.ixl.com/math/grade-4/multiply-a-2-digit-number-by-a-larger-number"
                },
                {
                    "name": "Model decimals and fractions",
                    "href": "https://www.ixl.com/math/grade-4/model-decimals-and-fractions"
                },
                {
                    "name": "Multi-step word problems",
                    "href": "https://www.ixl.com/math/grade-4/multi-step-word-problems"
                },
                {
                    "name": "Classify triangles",
                    "href": "https://www.ixl.com/math/grade-4/classify-triangles"
                },
                {
                    "name": "Multi-step word problems with money: addition and subtraction only",
                    "href": "https://www.ixl.com/math/grade-4/multi-step-word-problems-with-money-addition-and-subtraction-only"
                }
            ]
        },
        {
            "name": "Fifth grade",
            "abbr": "5",
            "short_description": "Multiplying fractions and decimals, idioms, prepositions, photosynthesis, molecules, economics, and more.",
            "short_skills": [
                {
                    "name": "Graph points on a coordinate plane",
                    "href": "https://www.ixl.com/math/grade-5/graph-points-on-a-coordinate-plane"
                },
                {
                    "name": "Evaluate numerical expressions",
                    "href": "https://www.ixl.com/math/grade-5/evaluate-numerical-expressions"
                },
                {
                    "name": "Add fractions with unlike denominators",
                    "href": "https://www.ixl.com/math/grade-5/add-fractions-with-unlike-denominators"
                },
                {
                    "name": "Volume of rectangular prisms made of unit cubes",
                    "href": "https://www.ixl.com/math/grade-5/volume-of-rectangular-prisms-made-of-unit-cubes"
                }
            ]
        },
        {
            "name": "Sixth grade",
            "abbr": "6",
            "short_description": "Ratios and percentages, variable expressions, Greek and Latin roots, genetics, ancient history, and more."
        },
        {
            "name": "Seventh grade",
            "abbr": "7",
            "short_description": "Proportional relationships, phrases and clauses, engineering design, world history, and more."
        },
        {
            "name": "Eighth grade",
            "abbr": "8",
            "short_description": "Linear functions, the Pythagorean theorem, active and passive voice, chemical formulas, civics, and more."
        },
        {
            "name": "Ninth grade",
            "abbr": "9",
            "short_description": "Quadratic equations, scatter plots, exponents, parallel language, figures of speech, semicolons, and more."
        },
        {
            "name": "Tenth grade",
            "abbr": "10",
            "short_description": "Congruent triangles, geometric constructions, colons, word patterns, audience and tone, and more."
        },
        {
            "name": "Eleventh grade",
            "abbr": "11",
            "short_description": "Trigonometric functions, logarithms, polynomials, supporting evidence, claims and counterclaims, and more."
        },
        {
            "name": "Twelfth grade",
            "abbr": "12",
            "short_description": "Complex numbers, derivatives, limits, domain-specific vocabulary, writing arguments, and more."
        }
    ];
    
    let grades = [];
    
    if (fromToArr) {
        grades = baseGrades.slice(fromToArr[0], fromToArr[1])
    } else {
        grades = baseGrades;
    }
    
    return grades;
}

export const state = () => ({})

export const getters = {
    databaseItems() {
        return [
            {
                name: 'math'
            },
            {
                name: 'language_arts',
            },
            {
                name: 'science',
            },
            {
                name: 'social_studies',
            },
        ]
    },
    subjects() {
        return [
            {
                name: 'Math'
            },
            {
                name: 'Language arts',
            },
            {
                name: 'Science',
            },
            {
                name: 'Social studies',
            },
        ]
    },
    mathGrades() {
        const mathGrades = [
            {
                "name": "Algebra 1",
                "abbr": "A1",
                "short_skills": [
                    {
                        "name": "Solve a system of equations by graphing",
                        "href": "https://www.ixl.com/math/algebra-1/solve-a-system-of-equations-by-graphing"
                    },
                    {
                        "name": "Match quadratic functions and graphs",
                        "href": "https://www.ixl.com/math/algebra-1/match-quadratic-functions-and-graphs"
                    },
                    {
                        "name": "Solve equations: complete the solution",
                        "href": "https://www.ixl.com/math/algebra-1/solve-equations-complete-the-solution"
                    },
                    {
                        "name": "Graph a two-variable linear inequality",
                        "href": "https://www.ixl.com/math/algebra-1/graph-a-two-variable-linear-inequality"
                    }
                ]
            },
            {
                "name": "Geometry",
                "abbr": "G",
                "short_skills": [
                    {
                        "name": "Prove similarity statements",
                        "href": "https://www.ixl.com/math/geometry/prove-similarity-statements"
                    },
                    {
                        "name": "Construct a perpendicular line",
                        "href": "https://www.ixl.com/math/geometry/construct-a-perpendicular-line"
                    },
                    {
                        "name": "SSS, SAS, ASA, and AAS Theorems",
                        "href": "https://www.ixl.com/math/geometry/sss-sas-asa-and-aas-theorems"
                    },
                    {
                        "name": "Special right triangles",
                        "href": "https://www.ixl.com/math/geometry/special-right-triangles"
                    },
                    {
                        "name": "Law of Cosines",
                        "href": "https://www.ixl.com/math/geometry/law-of-cosines"
                    },
                    {
                        "name": "Triangles and bisectors",
                        "href": "https://www.ixl.com/math/geometry/triangles-and-bisectors"
                    },
                    {
                        "name": "Checkpoint: Definitions of geometric objects",
                        "href": "https://www.ixl.com/math/geometry/checkpoint-definitions-of-geometric-objects"
                    }
                ]
            },
            {
                "name": "Algebra 2",
                "abbr": "A2",
                "short_skills": [
                    {
                        "name": "Match polynomials and graphs",
                        "href": "https://www.ixl.com/math/algebra-2/match-polynomials-and-graphs"
                    },
                    {
                        "name": "Graph sine and cosine functions",
                        "href": "https://www.ixl.com/math/algebra-2/graph-sine-and-cosine-functions"
                    },
                    {
                        "name": "Transformations of functions",
                        "href": "https://www.ixl.com/math/algebra-2/transformations-of-functions"
                    },
                    {
                        "name": "Graph a discrete probability distribution",
                        "href": "https://www.ixl.com/math/algebra-2/graph-a-discrete-probability-distribution"
                    },
                    {
                        "name": "Find the foci of an ellipse",
                        "href": "https://www.ixl.com/math/algebra-2/find-the-foci-of-an-ellipse"
                    }
                ]
            },
            {
                "name": "Precalculus",
                "abbr": "PC",
                "short_skills": [
                    {
                        "name": "Solve matrix equations using inverses",
                        "href": "https://www.ixl.com/math/precalculus/solve-matrix-equations-using-inverses"
                    },
                    {
                        "name": "Absolute values of complex numbers",
                        "href": "https://www.ixl.com/math/precalculus/absolute-values-of-complex-numbers"
                    },
                    {
                        "name": "Add vectors",
                        "href": "https://www.ixl.com/math/precalculus/add-vectors"
                    },
                    {
                        "name": "Find probabilities using the normal distribution I",
                        "href": "https://www.ixl.com/math/precalculus/find-probabilities-using-the-normal-distribution"
                    },
                    {
                        "name": "Radians and arc length",
                        "href": "https://www.ixl.com/math/precalculus/radians-and-arc-length"
                    }
                ]
            },
            {
                "name": "Calculus",
                "abbr": "C",
                "short_skills": [
                    {
                        "name": "Determine if a limit exists",
                        "href": "https://www.ixl.com/math/calculus/determine-if-a-limit-exists"
                    },
                    {
                        "name": "Make a piecewise function continuous",
                        "href": "https://www.ixl.com/math/calculus/make-a-piecewise-function-continuous"
                    },
                    {
                        "name": "Find tangent lines using implicit differentiation",
                        "href": "https://www.ixl.com/math/calculus/find-tangent-lines-using-implicit-differentiation"
                    },
                    {
                        "name": "Velocity as a rate of change",
                        "href": "https://www.ixl.com/math/calculus/velocity-as-a-rate-of-change"
                    },
                    {
                        "name": "Intermediate Value Theorem",
                        "href": "https://www.ixl.com/math/calculus/intermediate-value-theorem"
                    }
                ]
            }
        ];
        
        return [...grades([0, 7]), ...mathGrades];
    },
    learningArtsGrades() {
        return [...grades()]
    },
    scienceGrades() {
        return [...grades([3, 7])]
    },
    socialStudiesGrades() {
        return [...grades([3, 7])]
    },
    homePageGrades(state, getters) {
        let items = grades();
    
        items.forEach((grade) => {
    
            getters.mathGrades.forEach((item) => {
                if (item.name === grade.name) {
                    grade.math = true;
                    return false;
                }
            })
    
            getters.learningArtsGrades.forEach((item) => {
                if (item.name === grade.name) {
                    grade.learning = true;
                    return false;
                }
            })
    
            getters.scienceGrades.forEach((item) => {
                if (item.name === grade.name) {
                    grade.science = true;
                    return false;
                }
            })
    
            getters.socialStudiesGrades.forEach((item) => {
                if (item.name === grade.name) {
                    grade.social = true;
                    return false;
                }
            })
        })

        return items
    }
}

// let items = [];
//
// document.querySelectorAll('.category-skill-names').forEach(function (el) {
//     let data = {};
//     data.categoryName = el.querySelector('.header-text').textContent;
//
//     data.skills = []
//    el.querySelectorAll('.table-section-body .skill-name-cell .skill-name').forEach(function (skillName) {
//        data.skills.push(skillName.textContent)
//    })
//
//     items.push(data);
// })

// let items = [];
//
// let grades = [
//     {
//         name: 'Pre-K',
//         abbr: 'Pre-K',
//         short_description: 'Counting objects, inside and outside, longer and shorter, letter names, rhyming words, and more.'
//     },
//     {
//         name: 'Kindergarten',
//         abbr: 'K',
//         short_description: 'Comparing numbers, names of shapes, consonant and vowel sounds, sight words, and more.'
//     },
//     {
//         name: 'First grade',
//         abbr: '1',
//     },
//     {
//         name: 'Second grade',
//         abbr: '2',
//     },
//     {
//         name: 'Third grade',
//         abbr: '3',
//     }, {
//         name: 'Fourth grade',
//         abbr: '4',
//     }, {
//         name: 'Fifth grade',
//         abbr: '5',
//     }, {
//         name: 'Sixth grade',
//         abbr: '6',
//     }, {
//         name: 'Seventh grade',
//         abbr: '7',
//     }, {
//         name: 'Eighth grade',
//         abbr: '8',
//     },
//     {
//         name: 'Ninth grade',
//         abbr: '9',
//     },
//     {
//         name: 'Tenth grade',
//         abbr: '10',
//     },
//     {
//         name: 'Eleventh grade',
//         abbr: '11',
//     },
//     {
//         name: 'Twelfth grade',
//         abbr: '12',
//     },
// ];
//
// document.querySelectorAll('.grade-list-item').forEach(function (el) {
//     let data = {};
//     data.name = el.querySelector('.grade-box-long-name') ? el.querySelector('.grade-box-long-name').textContent : null;
//     data.short_description = el.querySelector('.grade-description') ? el.querySelector('.grade-description').textContent : null;
//
//     if (data.name) {
//         items.push(data);
//     }
// })
//
// let items = [];
// let grades = [{"name":"Pre-K","abbr":"Pre-K","short_description":"Counting objects, inside and outside, longer and shorter, letter names, rhyming words, and more."},{"name":"Kindergarten","abbr":"K","short_description":"Comparing numbers, names of shapes, consonant and vowel sounds, sight words, and more."},{"name":"First grade","abbr":"1","short_description":"Adding and subtracting, tens and ones, telling time, categories, nouns, verb tense, time order, and more."},{"name":"Second grade","abbr":"2","short_description":"Place-value models, contractions, irregular plurals, plants and animals, historical figures, and more."},{"name":"Third grade","abbr":"3","short_description":"Multiplying and dividing, bar graphs, pronouns, possessives, weather and climate, geography, and more."},{"name":"Fourth grade","abbr":"4","short_description":"Fractions and decimals, synonyms and antonyms, fossils and rock layers, government, and more."},{"name":"Fifth grade","abbr":"5","short_description":"Multiplying fractions and decimals, idioms, prepositions, photosynthesis, molecules, economics, and more."},{"name":"Algebra 1","abbr":"A1"},{"name":"Geometry","abbr":"G"},{"name":"Algebra 2","abbr":"A2"},{"name":"Precalculus","abbr":"PC"},{"name":"Calculus","abbr":"C"}];
//
// document.querySelectorAll('.ancestor-node.grade-node').forEach(function (el) {
//     let data = {};
//     data.name = el.querySelector('.ancestor-node-header .node-name ') ? el.querySelector('.ancestor-node-header .node-name ').textContent.trim() : null;
//     data.code = el.querySelector('.ancestor-node-label') ? el.querySelector('.ancestor-node-label').textContent.trim() : null;
//     // data.items = []
//     //
//     // el.querySelectorAll('.child-item').forEach((childItem) => {
//     //     const link = childItem.querySelector('.skill-tree-skill-link');
//     //
//     //     if (link) {
//     //         data.items.push({
//     //             name: link.textContent.replace('\n', '').trim(),
//     //             href: link.href
//     //         })
//     //     }
//     //
//     // })
//
//     if (data.name) {
//         items.push(data);
//     }
// })
//
// grades.forEach((grade, index) => {
//     items.forEach(item => {
//         if (item.name === grade.name) {
//             grades[index].short_skills= item.items
//         }
//     })
// })


