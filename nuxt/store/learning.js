

export const getters = {
    schemeSubRoutes (state, getters, rootState, rootGetters) {
        const routes = rootGetters['settings/routes'].learning.children;
        const auth = rootState['auth'];
        
        const role = auth.loggedIn ? auth.user.role : null
        
        let subRoutes = [
            {
                route: routes.learningMath,
                title: 'Math',
                imgSrc: '/images/achievements/math.png'
            },
    
            {
                route: routes.learningLanguageArts,
                title: 'Language arts',
                imgSrc: '/images/achievements/la.png'
            },
            {
                route: routes.learningSocialStudies,
                title: 'Social studies',
                imgSrc: '/images/sub_navbar_icons/diagnostic.png'
            },
            {
                route: routes.learningScience,
                title: 'Science',
                imgSrc: '/images/sub_navbar_icons/students.png'
            },
        ];
        
        return _.filter(subRoutes, (item) => {
            return item.route;
        })
    },
    baseContentView (state, getters, rootState) {
        return ! getters['schemeSubRoutes'].length
    }
}