

export const getters = {
    schemeSubRoutes (state, getters, rootState, rootGetters) {
        const routes = rootGetters['settings/routes'].analytics.children;
        const auth = rootState['auth'];
        
        const role = auth.loggedIn ? auth.user.role : null
        
        let subRoutes = [];
        
        switch (role) {
            case 'teacher':
                subRoutes =  [
                    {
                        route: routes.analyticsDashboard,
                        title: 'Dashboard',
                        imgSrc: '/images/sub_navbar_icons/dashboard.png'
                    },
                    {
                        route: routes.analyticsDiagnostic,
                        title: 'Diagnostic',
                        imgSrc: '/images/sub_navbar_icons/diagnostic.png'
                    },
                    {
                        route: routes.analyticsTroubleSpots,
                        title: 'Trouble spots',
                        imgSrc: '/images/sub_navbar_icons/trouble-spots.png'
                    },
                    {
                        route: routes.analyticsScores,
                        title: 'Scores',
                        imgSrc: '/images/sub_navbar_icons/score.png'
                    },
                    {
                        route: routes.analyticsStudents,
                        title: 'Students',
                        imgSrc: '/images/sub_navbar_icons/students.png'
                    },
                    {
                        route: routes.analyticsProgress,
                        title: 'Progress & Growth',
                        imgSrc: '/images/sub_navbar_icons/progress.png'
                    },
                ];
                break;
            case 'parent':
                subRoutes = [
                    {
                        route: routes.analyticsDashboard,
                        title: 'Usage',
                        imgSrc: '/images/sub_navbar_icons/students.png'
                    },
                    {
                        route: routes.analyticsDiagnostic,
                        title: 'Diagnostic',
                        imgSrc: '/images/sub_navbar_icons/diagnostic.png'
                    },
                    {
                        route: routes.analyticsTroubleSpots,
                        title: 'Trouble spots',
                        imgSrc: '/images/sub_navbar_icons/trouble-spots.png'
                    },
                    {
                        route: routes.analyticsScores,
                        title: 'Scores',
                        imgSrc: '/images/sub_navbar_icons/score.png'
                    },
                    {
                        route: routes.analyticsProgress,
                        title: 'Progress',
                        imgSrc: '/images/sub_navbar_icons/progress.png'
                    },
                ];
                break;
            case 'student':
                subRoutes = [
                    {
                        route: routes.analyticsStudents,
                        title: 'Usage',
                        imgSrc: '/images/sub_navbar_icons/students.png'
                    },
                    {
                        route: routes.analyticsTroubleSpots,
                        title: 'Trouble spots',
                        imgSrc: '/images/sub_navbar_icons/trouble-spots.png'
                    },
                    {
                        route: routes.analyticsScores,
                        title: 'Scores',
                        imgSrc: '/images/sub_navbar_icons/score.png'
                    },
                    {
                        route: routes.analyticsProgressSingleStudent.params({ studentId: auth.user.id}),
                        title: 'Progress',
                        imgSrc: '/images/sub_navbar_icons/progress.png'
                    },
                ];
                break;
            case 'admin':
                subRoutes = [
                    {
                        route: routes.analyticsDiagnostic,
                        title: 'Diagnostic',
                        imgSrc: '/images/sub_navbar_icons/diagnostic.png'
                    },
                    {
                        route: routes.analyticsTroubleSpots,
                        title: 'Trouble spots',
                        imgSrc: '/images/sub_navbar_icons/trouble-spots.png'
                    },
                    {
                        route: routes.analyticsScores,
                        title: 'Scores',
                        imgSrc: '/images/sub_navbar_icons/score.png'
                    },
                    {
                        route: routes.analyticsStudents,
                        title: 'Students',
                        imgSrc: '/images/sub_navbar_icons/students.png'
                    },
                    {
                        route: routes.analyticsProgress,
                        title: 'Progress & Growth',
                        imgSrc: '/images/sub_navbar_icons/progress.png'
                    },
                ];
                break;
        }
        
        return _.filter(subRoutes, (item) => {
            return item.route;
        })
    },
    baseContentView (state, getters, rootState) {
        return ! getters['schemeSubRoutes'].length
    }
}