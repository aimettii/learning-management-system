export const state = () => ({
    brand: 'Codeaily',
    docsHostname: 'lms.codeaily.com'
})

export const getters = {
    settings: state => state.settings.state
}

// export const strict = false