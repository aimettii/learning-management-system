export const state = () => ({
    list: {
        items: [],
        meta: {}
    },
    show: null,
    showLoading: false
})

export const mutations = {
    SET_LIST (state, payload) {
        state.list.items = payload.data
        state.list.meta = payload.meta
    },
    CLEAR_LIST (state) {
        state.list.items = []
        state.list.meta = {}
    },
    SET_SHOW (state, payload) {
        state.show = payload
    },
    SET_SHOW_LOADING (state, payload) {
        state.showLoading = payload
    },
}

const images = ['/images/paths/angular_testing_200x168.png', '/images/paths/angular_routing_200x168.png', '/images/paths/typescript_200x168.png', '/images/paths/angular_200x168.png'];

export const getters = {
    listItems (state) {
        // // @todo harcode images
        let i = -1;
        
        return state.list.items.map((item, index) => {
            i++;

            if (!images[i]) {
                i = 0;
            }

            return {
                ...item,
                image: images[i],
            }
        });
    },
    listMeta (state) {
        return state.list.meta
    },
    showItem (state) {
        return state.show && {
            ...state.show,
            image: images[Math.floor(Math.random()*images.length)]
        }
    }
}
