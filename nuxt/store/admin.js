export const getters = {
    schemeSubRoutes (state, getters, rootState, rootGetters) {
        const routes = rootGetters['settings/routes'].adminDashboard ? rootGetters['settings/routes'].adminDashboard.children : {};
        
        let subRoutes = [
            {
                route: routes.adminSkillsList,
                title: 'Skills',
                imgSrc: '/images/sub_navbar_icons/question.png'
            },
            {
                route: routes.adminUsersList,
                title: 'Users',
                imgSrc: '/images/sub_navbar_icons/students.png'
            },
        ];
        
        return _.filter(subRoutes, (item) => {
            return item.route;
        })
    },
    baseContentView (state, getters, rootState) {
        return ! getters['schemeSubRoutes'].length
    }
}