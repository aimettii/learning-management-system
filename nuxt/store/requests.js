import Vue from 'vue';

export const state = () => ({
    modelActiveRequests: [],
    swalActive: false
});

export const mutations = {
    ADD_ACTIVE_REQUEST (state, request) {
        state.modelActiveRequests.push(request);
    },
    SET_SWAL_STATE(state, boolean) {
        state.swalActive = boolean;
    },
    REMOVE_ACTIVE_REQUEST (state, id) {
        const index = _.findIndex(state.modelActiveRequests, { id })
    
        index && state.modelActiveRequests.splice(index, 1);
    },
    CLEAR_ACTIVE_REQUESTS (state) {
        state.modelActiveRequests = [];
    }
}

export const actions = {
    addActiveRequest({commit, state}, request) {
        const id = _.uniqueId();
        commit('ADD_ACTIVE_REQUEST', {
            id,
            promise: request.catch((error) => {
                if (! state.swalActive && !!_.find(state.modelActiveRequests, ['id', id])) {
                    commit('SET_SWAL_STATE', true)
                    Vue.swal.fire({
                        title: 'An unexpected error occurred while requesting.',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
                        commit('SET_SWAL_STATE', false)
                        commit('CLEAR_ACTIVE_REQUESTS')
                    })
                }
                return Promise.reject(error)
            }).then((response) => {
                commit('REMOVE_ACTIVE_REQUEST', id)
                return Promise.resolve(response)
            })
        })
    }
}