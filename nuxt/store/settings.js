const _ = require('lodash')

export const state = () => ({
    ui: false,
    state: {
        layout: {
            layout: 'fixed',
            rtl: false,
        },
        'app::mainDrawer': {
            align: 'start',
            sidebar: 'dark',
        },
        'fixed::mainDrawer': {
            align: 'start',
            sidebar: 'dark',
        },
        'sticky::mainDrawer': {
            align: 'start',
            sidebar: 'dark',
        },
        'boxed::mainDrawer': {
            align: 'start',
            sidebar: 'light',
        },
        'app::mainNavbar': {
            navbar: 'light',
        },
        'fixed::mainNavbar': {
            navbar: 'dark',
        },
        'sticky::mainNavbar': {
            navbar: 'light',
        },
        'boxed::mainNavbar': {
            navbar: 'dark',
        },
    },
    router: {
        last: null
    }
})

export const mutations = {
    SET_SETTINGS (state, settings) {
        if (settings) {
            state.state = _.merge({}, state.state, settings)
        }
    },
    SET_LAST_ROUTE (state, payload) {
        state.router.last = payload
    }
}

export const actions = {
    setSettings ({ commit }, settings) {
        commit('SET_SETTINGS', settings)
    },
}

const LOGIN_ROUTE = 'login'
const SIGN_UP_ROUTE = 'sign_up'
const PROFILE_ROUTE = 'profile'
const HOME_ROUTE = 'home'
const LEARNING_ROUTE = 'learning'
const LEARNING_MATH_ROUTE = 'learning_math'
const LEARNING_SOCIAL_STUDIES_ROUTE = 'learning_social_studies'
const LEARNING_SCIENCE_ROUTE = 'learning_science'
const LEARNING_LANGUAGE_ARTS_ROUTE = 'learning_language_arts'
const DIAGNOSTIC_ROUTE = 'diagnostic'
const ANALYTICS_ROUTE = 'analytics'
const ANALYTICS_DASHBOARD_ROUTE = 'analytics_dashboard'
const ANALYTICS_DIAGNOSTIC_ROUTE = 'analytics_diagnostic'
const ANALYTICS_SCORES_ROUTE = 'analytics_scores'
const ANALYTICS_STUDENTS_ROUTE = 'analytics_students'
const ANALYTICS_PROGRESS_ROUTE = 'analytics_progress'
const ANALYTICS_PROGRESS_SINGLE_STUDENT_ROUTE = 'analytics_progress_single_student'
const ANALYTICS_TROUBLE_SPOTS_ROUTE = 'analytics_trouble_spots'

const ADMIN_DASHBOARD_ROUTE = 'admin_dashboard'
const ADMIN_SKILLS_LIST_ROUTE = 'admin_skills_list'
const ADMIN_SKILLS_LIST_SUBJECT_ROUTE = 'admin_skills_list_subject'
const ADMIN_SKILLS_LIST_SUBJECT_GRADE_ROUTE = 'admin_skills_list_subject_grade'
const ADMIN_SKILLS_CREATE_ROUTE = 'admin_skills_create'
const ADMIN_SKILLS_EDIT_ROUTE = 'admin_skills_edit'
const ADMIN_USERS_LIST_ROUTE = 'admin_users_list'
const BILLING_ROUTE = 'billing'

const ALL_ROUTES = {
    [LOGIN_ROUTE]: {
        path: '/auth/login',
    },
    [SIGN_UP_ROUTE]: {
        path: '/auth/signup',
    },
    [PROFILE_ROUTE]: {
        path: '/profile',
        roles: ['teacher', 'parent', 'student']
    },
    [BILLING_ROUTE]: {
        path: '/billing',
        roles: ['teacher', 'parent']
    },
    [HOME_ROUTE]: {
        path: '/home',
    },
    [LEARNING_ROUTE]: {
        path: '/learning',
        children: {
            [LEARNING_MATH_ROUTE]: {
                path: '/learning/math',
            },
            [LEARNING_SOCIAL_STUDIES_ROUTE]: {
                path: '/learning/social-studies',
            },
            [LEARNING_SCIENCE_ROUTE]: {
                path: '/learning/science',
            },
            [LEARNING_LANGUAGE_ARTS_ROUTE]: {
                path: '/learning/language-arts',
            },
        }
    },
    [DIAGNOSTIC_ROUTE]: {
        path: '/diagnostic',
        roles: ['student', 'teacher', 'parent', null]
    },
    [ANALYTICS_ROUTE]: {
        path: '/analytics',
        children: {
            [ANALYTICS_DASHBOARD_ROUTE]: {
                path: '/analytics/dashboard',
                roles: ['teacher', 'parent']
            },
            [ANALYTICS_DIAGNOSTIC_ROUTE]: {
                path: '/analytics/diagnostic',
                roles: ['teacher', 'parent', 'admin']
            },
            [ANALYTICS_SCORES_ROUTE]: {
                path: '/analytics/scores',
                roles: ['teacher', 'parent', 'admin', 'student']
            },
            [ANALYTICS_STUDENTS_ROUTE]: {
                path: '/analytics/students',
                roles: ['teacher', 'parent', 'admin', 'student']
            },
            [ANALYTICS_PROGRESS_ROUTE]: {
                path: '/analytics/progress-growth',
                roles: ['teacher', 'parent', 'admin']
            },
            [ANALYTICS_PROGRESS_SINGLE_STUDENT_ROUTE]: {
                path: '/analytics/progress-growth/:studentId',
                roles: ['teacher', 'parent', 'admin', 'student']
            },
            [ANALYTICS_TROUBLE_SPOTS_ROUTE]: {
                path: '/analytics/trouble-spots',
                roles: ['teacher', 'parent', 'admin', 'student']
            },
            
        }
    },
    [ADMIN_DASHBOARD_ROUTE]: {
        path: '/admin',
        roles: ['admin'],
        children: {
            [ADMIN_SKILLS_LIST_ROUTE]: {
                path: '/admin/skills',
                children: {
                    [ADMIN_SKILLS_LIST_SUBJECT_ROUTE]: {
                        path: '/admin/skills/:subject',
                    },
                    [ADMIN_SKILLS_LIST_SUBJECT_GRADE_ROUTE]: {
                        path: '/admin/skills/:subject/:grade',
                    },
                }
            },
            [ADMIN_SKILLS_CREATE_ROUTE]: {
                path: '/admin/skills/create',
            },
            [ADMIN_SKILLS_EDIT_ROUTE]: {
                path: '/admin/skills/:shortcut/edit',
            },
            [ADMIN_USERS_LIST_ROUTE]: {
                path: '/admin/users',
            },
        }
    }
}

const itterationRoutesForRole = (routes, role) => {
    let filteredRoutes = {};
    
    _.forIn(routes, (value, key) => {
        let clonedObject = Object.assign({}, _.cloneDeep(value));
    
        clonedObject.params = function(obj) {
            _.forIn(obj, (param, paramKey) => {
                clonedObject.path = clonedObject.path.replace(`:${paramKey}`, param);
            })
            
            return Object.assign({}, _.cloneDeep(clonedObject));
        }
       
        if (clonedObject.roles) {
            if (! clonedObject.roles.includes(role)) {
                return;
            }
        }
        
        if (clonedObject.children) {
            const children = itterationRoutesForRole(clonedObject.children, role);
            clonedObject = _.merge(clonedObject, children);
            clonedObject.children = children;
        }
    
        filteredRoutes[_.camelCase(key)] = clonedObject;
    })
    
    return filteredRoutes;
}

export const getters = {
    layout: state => state.state.layout.layout,
    routes: (state, getters, rootState) => {
        const auth = rootState['auth'];
        
        return itterationRoutesForRole(ALL_ROUTES, auth.loggedIn ? auth.user.role : null);
    },
}
