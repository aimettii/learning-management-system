

import Vue from 'vue'

/**
 * Vue loader
 */
import 'vue-loaders/dist/vue-loaders.css';
import VueLoaders from 'vue-loaders';
Vue.use(VueLoaders);

/**
 * Global Helpers
 */
import StateIndicate from '~/components/Utils/StateIndicate'
import StateIndicateObserver from '../components/Utils/StateIndicateObserver'
Vue.component('state-indicate', StateIndicate)
Vue.component('state-indicate-observer', StateIndicateObserver);

//Custom Loaders
import ColorsCirclesStrokeLoader
    from '~/components/Ui/Loaders/ColorsCirclesStrokeLoader'
Vue.component('ColorsCirclesStrokeLoader', ColorsCirclesStrokeLoader)

import VueBsDrawer from 'vue-bs-drawer'

Vue.component('bs-drawer', VueBsDrawer)

import {mapGetters} from 'vuex'

/* BreakpointUtil.js */
const state = Vue.observable({
    screen: {}
});

/* This assumes you're using default bootstrap breakpoint names */
/* You need to hardcode the breakpoint values if you want to support IE11 */
const style = getComputedStyle(document.body);
const xs = style.getPropertyValue("--breakpoint-xs").replace("px", "");
const sm = style.getPropertyValue("--breakpoint-sm").replace("px", "");
const md = style.getPropertyValue("--breakpoint-md").replace("px", "");
const lg = style.getPropertyValue("--breakpoint-lg").replace("px", "");
const xl = style.getPropertyValue("--breakpoint-xl").replace("px", "");

function onResize() {
    const width = window.innerWidth;
    
    /* Not really sure how to properly define gt or lt */
    state.screen = {
        xs: width >= xs && width < sm,
        sm: width >= sm && width < md,
        md: width >= md && width < lg,
        lg: width >= lg && width < xl,
        xl: width >= xl,
        /* Greater than */
        gt: {
            xs: width >= xs,
            sm: width >= sm,
            md: width >= md,
            lg: width >= lg,
            xl: width >= xl
        },
        /* Less than */
        lt: {
            xs: width < sm,
            sm: width < md,
            md: width < lg,
            lg: width < xl,
            xl: width < 9999
        }
    };
}

/* Might want to debounce the event, to limit amount of calls */
window.onresize = onResize;
onResize();
/* BreakpointUtil.js END */

Vue.mixin({
    filters: {
        capitalize (val) {
            return _.capitalize(val)
        },
        subjectCapitalize (val) {
            return _.startCase(val.replace('_', ' '));
        },
    },
    methods: {
        _isEqual(val1, val2) {
            return _.isEqual(val1, val2)
        }
    },
    computed: {
        ...mapGetters('settings', [
            'routes',
        ]),
        screen: () => state.screen
    },
})