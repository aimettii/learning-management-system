import { Model } from 'vue-api-query'

import Vue from 'vue'

export default function (ctx, inject) {
    Model.$http = ctx.$axios
    
    Model.$http.interceptors.response.use(function (response) {
        return response;
    }, function ({response}) {
        if (response.data.message === "Unauthenticated.") {
            ctx.$auth.$storage.setState('_unauthenticated', true)
            ctx.redirect(ctx.store.getters['settings/routes'].login);
            return;
        }
        
        return Promise.reject(response.data);
    });
    
    const path = require.context('~/models', false, /\.js$/);
    
    const models = {}
    
    path.keys().filter((n) => !n.includes('Model.js')).map((n) => {
        return {
            name: n.replace(/(.+)\/(.+).js$/, '$2'),
            cb: path(n)
        }
    }).forEach(obj => {
        models[obj.name] = class extends obj.cb.default{
            constructor (...args) {
                super(...args);
            }
            
            get $nuxt() {
                return ctx;
            }
        };
    })
   
    Vue.mixin({
        computed: {
            $models() {
                return models;
            }
        },
    })
    // Vue.prototype.$models = models
    inject('models', models);
}