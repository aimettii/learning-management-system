import ImportCDNJS from 'import-cdn-js';

ImportCDNJS('https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.6.7/fabric.min.js')
ImportCDNJS('https://cdn.jsdelivr.net/npm/tui-code-snippet@1.4.0/dist/tui-code-snippet.min.js')
ImportCDNJS('https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.min.js')
require('froala-editor/css/third_party/image_tui.min.css')

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min.js')
require('froala-editor/js/plugins.pkgd.min.js')
setTimeout(() => {
    require('froala-editor/js/third_party/image_tui.min.js')
   
}, 2000)

// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('froala-editor/css/froala_style.min.css')

// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg';

import Vue from 'vue'

Vue.use(VueFroala)


