export default function ({ $axios }, inject) {
    $axios.defaults.timeout = 10000;
    $axios.onRequest(config => {
        config.withCredentials = true
        
        return config
    })
}