export default function (context) {
    context.app.router.beforeEach(async (to, from, next) => {
        context.store.commit('settings/SET_LAST_ROUTE', from);
        next()
    });
}