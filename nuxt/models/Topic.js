import Model from './Model'

export default class Topic extends Model {
    // Set the resource route of the model
    resource() {
        return 'topics'
    }
    
    bySubject(Subject) {
        return this.custom(Subject, 'topics');
    }
}