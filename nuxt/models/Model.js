import { Model as BaseModel } from 'vue-api-query'

let throttleRequest = {}

export default class Model extends BaseModel {
    constructor (...args) {
        super(...args)
        this.requestIds = {};
        this.throttleKey = false;
    }
    
    // Define a base url for a REST API
    baseURL() {
        return '';
    }
    
    throttle(key) {
        this.throttleKey = key;
        if (!throttleRequest[this.throttleKey]) {
            throttleRequest[this.throttleKey] = []
        }
        return this;
    }
    
    // Implement a default request method
    request(config) {
        config.withCredentials = true;
        
        const request = this.$http.request(config);

        this.$nuxt.store.dispatch('requests/addActiveRequest', request);
        
        if (this.throttleKey) {
            let uniqueId = _.uniqueId();
            
            const currentRequest = {
                promise: request,
                id: uniqueId
            };
            
            throttleRequest[this.throttleKey].push(currentRequest)
    
            return new Promise((resolve, reject) => {
                const cb = (next, response) => {
                    if (throttleRequest[this.throttleKey] && throttleRequest[this.throttleKey].length > 1) {
                        if (currentRequest.id === _.last(throttleRequest[this.throttleKey]).id) {
                            delete throttleRequest[this.throttleKey]
                            next(response)
                        }
                    } else {
                        if (throttleRequest[this.throttleKey]) {
                            delete throttleRequest[this.throttleKey]
                            next(response)
                        }
                    }
                }
                currentRequest.promise.then((response) => cb(resolve, response)).catch((response) => cb(reject, response))
            });
        } else {
            return request;
        }
    }
}