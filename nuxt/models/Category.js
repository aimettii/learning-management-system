import Model from './Model'
import Topic from './Topic'

export default class Category extends Model {
    // Set the resource route of the model
    resource() {
        return 'categories'
    }
    
    byTopic(Topic) {
        return this.custom(`topics/${Topic.slug}/categories`);
    }
}