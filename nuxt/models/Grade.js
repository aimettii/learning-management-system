import Model from './Model'

export default class Grade extends Model {
    // Set the resource route of the model
    resource() {
        return 'grades'
    }
    
    bySubject(Subject) {
        return this.custom(Subject, 'grades');
    }
}