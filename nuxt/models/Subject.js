import Model from './Model'

export default class Subject extends Model {
    // Set the resource route of the model
    resource() {
        return 'subjects'
    }
    
    primaryKey() {
        return 'name'
    }
}