import Model from './Model'

export default class Skill extends Model {
    // Set the resource route of the model
    resource() {
        return 'skills'
    }
    
    get() {
        return super.get().then((response) => {
                this.$nuxt.store.commit('skills/SET_LIST', response)
                return response;
            });
    }
    
    find(...args) {
        this.$nuxt.store.commit('skills/SET_SHOW_LOADING', true);
        return super.find(...args).then((response) => {
            this.$nuxt.store.commit('skills/SET_SHOW', response.data)
            return response;
        }).finally(() =>  this.$nuxt.store.commit('skills/SET_SHOW_LOADING', false))
    }
}