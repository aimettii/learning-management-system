<?php

namespace Database\Seeders;

use App\Models\Question;
use App\Models\Skill;
use App\Models\Subject;
use App\Models\Topic;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\DB;

class TopicsSeeder extends Seeder
{
    public $topicsPreModels = [];
    public $skillsPreModels = [];
    public $questionsPreModels = [];
    public $lastTopicId;
    public $lastSkillId;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mathTopics = [
            [
                "name" => "Addition"
            ],
            [
                "name" => "Algebra"
            ],
            [
                "name" => "Comparing"
            ],
            [
                "name" => "Counting"
            ],
            [
                "name" => "Decimals"
            ],
            [
                "name" => "Division"
            ],
            [
                "name" => "Estimation"
            ],
            [
                "name" => "Exponents, roots, and logarithms"
            ],
            [
                "name" => "Fractions"
            ],
            [
                "name" => "Functions and equations"
            ],
            [
                "name" => "Geometry and spatial reasoning"
            ],
            [
                "name" => "Graphs"
            ],
            [
                "name" => "Integers"
            ],
            [
                "name" => "Logic and reasoning"
            ],
            [
                "name" => "Measurement"
            ],
            [
                "name" => "Mixed operations"
            ],
            [
                "name" => "Money and consumer math"
            ],
            [
                "name" => "Multiplication"
            ],
            [
                "name" => "Number theory"
            ],
            [
                "name" => "Patterns"
            ],
            [
                "name" => "Percents"
            ],
            [
                "name" => "Place values"
            ],
            [
                "name" => "Probability and statistics"
            ],
            [
                "name" => "Properties"
            ],
            [
                "name" => "Ratios and proportions"
            ],
            [
                "name" => "Subtraction"
            ],
            [
                "name" => "Time"
            ],
            [
                "name" => "Trigonometry"
            ],
            [
                "name" => "Word problems"
            ]
        ];

        $languageArtsTopics = [
            [
                "name" => "Adjectives and adverbs"
            ],
            [
                "name" => "Capitalization"
            ],
            [
                "name" => "Context clues"
            ],
            [
                "name" => "Editing and revising"
            ],
            [
                "name" => "Figurative language"
            ],
            [
                "name" => "Grammar"
            ],
            [
                "name" => "Informational texts"
            ],
            [
                "name" => "Literary texts"
            ],
            [
                "name" => "Main idea"
            ],
            [
                "name" => "Nouns"
            ],
            [
                "name" => "Opinions and arguments"
            ],
            [
                "name" => "Phonics"
            ],
            [
                "name" => "Phonological awareness"
            ],
            [
                "name" => "Pronouns"
            ],
            [
                "name" => "Punctuation"
            ],
            [
                "name" => "Reading comprehension"
            ],
            [
                "name" => "Reference skills"
            ],
            [
                "name" => "Roots and affixes"
            ],
            [
                "name" => "Spelling"
            ],
            [
                "name" => "Verbs"
            ],
            [
                "name" => "Vocabulary"
            ],
            [
                "name" => "Word study"
            ],
            [
                "name" => "Writing strategies"
            ]
        ];

        $scienceTopics = [
            [
                "name" => "Biology"
            ],
            [
                "name" => "Chemistry"
            ],
            [
                "name" => "Earth science"
            ],
            [
                "name" => "Physics"
            ],
            [
                "name" => "Science and engineering practices"
            ],
            [
                "name" => "Literacy in science"
            ],
            [
                "name" => "Units and measurement"
            ]
        ];

        $socialStudies =  [
            [
                "name" => "Geography"
            ],
            [
                "name" => "Economics"
            ],
            [
                "name" => "Culture"
            ],
            [
                "name" => "Civics"
            ],
            [
                "name" => "U.S. history"
            ],
            [
                "name" => "World history"
            ],
            [
                "name" => "Global studies"
            ]
        ];

        $this->lastTopicId = DB::table('topics')->max('id') ?? 0;
        $this->lastSkillId = DB::table('skills')->max('id') ?? 0;
        $this->getData($mathTopics, Subject::MATH);
        $this->getData($languageArtsTopics, Subject::LANGUAGE_ARTS);
        $this->getData($scienceTopics, Subject::SCIENCE);
        $this->getData($socialStudies, Subject::SOCIAL_STUDIES);

        Topic::insert($this->topicsPreModels);
        Skill::insert($this->skillsPreModels);
        Question::insert($this->questionsPreModels);
    }

    public function getData($topics, $subjectName)
    {
        $subject = Subject::where('name', $subjectName)->firstOrFail();

        $grades  = $subject->grades;

        foreach ($topics as $topic) {
            $this->lastTopicId++;
            $this->topicsPreModels[] = array_merge($topic, [
                'id' => $this->lastTopicId,
                'subject_id' => $subject->id,
                'slug' => SlugService::createSlug(Topic::class, 'slug', $topic['name']),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            foreach ($grades as $grade) {
                $this->lastSkillId++;

                $this->skillsPreModels[] = Skill::factory(1)->topic($topic['name'])->makeOne([
                    'id' => $this->lastSkillId,
                    'shortcut' => Skill::generateShortcut($this->lastSkillId),
                    'grade_id' => $grade->id,
                    'topic_id' => $this->lastTopicId,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ])->toArray();

                $this->questionsPreModels = array_merge($this->questionsPreModels, Question::factory(5)->make([
                    'skill_id' => $this->lastSkillId,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ])->map(function($item) { return $item->toArray(); })->all());
            }
        }
    }
}
