<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
//        Permission::create(['name' => 'edit articles']);


        // create roles and assign created permissions

        // this can be done as separate statements
        $roles = Role::getAllRoles();

        foreach ($roles as $role) {
            Role::findOrCreate($role);
        }
    }
}
