<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->role(Role::ADMIN)->create(['email' => 'admin@example.com']);
        User::factory()->role(Role::TEACHER)->create(['email' => 'teacher@example.com']);
        User::factory()->role(Role::STUDENT)->create(['email' => 'student@example.com']);
        User::factory()->role(Role::PARENT)->create(['email' => 'parent@example.com']);
//        User::factory(100)->role(Role::CUSTOMER)->create();
    }
}
