<?php

namespace Database\Seeders;

use App\Models\Skill;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skill::factory(2000)->topic('asdasd')->create([
            'grade_id' => 1,
            'topic_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ])->toArray();
    }
}
