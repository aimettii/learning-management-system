<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call("scout:flush", ['model' => "App\Models\Skill"]);
        Artisan::call("scout:flush", ['model' => "App\Models\Topic"]);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(ProfileAttributesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(SubjectsSeeder::class);
        $this->call(GradesSeeder::class);
        $this->call(TopicsSeeder::class);
        Artisan::call("scout:import", ['model' => "App\Models\Skill"]);
        Artisan::call("scout:import", ['model' => "App\Models\Topic"]);
    }
}
