<?php

namespace Database\Seeders;

use App\Models\ProfileAttribute;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileAttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $baseAttributes = [];

        foreach (ProfileAttribute::getAllAttributes() as $attribute)
        {
            $profileAttribute = ProfileAttribute::create(['name' => $attribute]);

            $baseAttributes[] = [
              'profile_attribute_id' => $profileAttribute->id,
              'is_required' => in_array($attribute, [ ProfileAttribute::FIRST_NAME, ProfileAttribute::LAST_NAME ]),
            ];
        }

        foreach (Role::all() as $role) {
            if (! in_array($role->name, [Role::TEACHER, Role::STUDENT, Role::PARENT])) {
                continue;
            }

            $attributes = collect($baseAttributes)->map(function ($attribute) use ($role){
                return array_merge($attribute, ['role_id' =>  $role->id]);
            })->all();

            DB::table('role_profile_attributes')->insert($attributes);
        }
    }
}
