<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert(
            collect(Subject::getAllSubjects())->map(function ($subject) {
                return [
                    'name' => $subject
                ];
            })->all()
        );
    }
}
