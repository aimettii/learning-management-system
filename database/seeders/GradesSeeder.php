<?php

namespace Database\Seeders;

use App\Models\Grade;
use App\Models\Subject;
use Illuminate\Database\Seeder;

class GradesSeeder extends Seeder
{
    public $allGradesWithShortDescription;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->allGradesWithShortDescription = [
            [
                "name" => "Pre-K",
                "abbr" => "Pre-K",
                "short_description" => "Counting objects, inside and outside, longer and shorter, letter names, rhyming words, and more."
            ],
            [
                "name" => "Kindergarten",
                "abbr" => "K",
                "short_description" => "Comparing numbers, names of shapes, consonant and vowel sounds, sight words, and more."
            ],
            [
                "name" => "First grade",
                "abbr" => "1",
                "short_description" => "Adding and subtracting, tens and ones, telling time, categories, nouns, verb tense, time order, and more."
            ],
            [
                "name" => "Second grade",
                "abbr" => "2",
                "short_description" => "Place-value models, contractions, irregular plurals, plants and animals, historical figures, and more."
            ],
            [
                "name" => "Third grade",
                "abbr" => "3",
                "short_description" => "Multiplying and dividing, bar graphs, pronouns, possessives, weather and climate, geography, and more."
            ],
            [
                "name" => "Fourth grade",
                "abbr" => "4",
                "short_description" => "Fractions and decimals, synonyms and antonyms, fossils and rock layers, government, and more."
            ],
            [
                "name" => "Fifth grade",
                "abbr" => "5",
                "short_description" => "Multiplying fractions and decimals, idioms, prepositions, photosynthesis, molecules, economics, and more."
            ],
            [
                "name" => "Sixth grade",
                "abbr" => "6",
                "short_description" => "Ratios and percentages, variable expressions, Greek and Latin roots, genetics, ancient history, and more."
            ],
            [
                "name" => "Seventh grade",
                "abbr" => "7",
                "short_description" => "Proportional relationships, phrases and clauses, engineering design, world history, and more."
            ],
            [
                "name" => "Eighth grade",
                "abbr" => "8",
                "short_description" => "Linear functions, the Pythagorean theorem, active and passive voice, chemical formulas, civics, and more."
            ],
            [
                "name" => "Ninth grade",
                "abbr" => "9",
                "short_description" => "Quadratic equations, scatter plots, exponents, parallel language, figures of speech, semicolons, and more."
            ],
            [
                "name" => "Tenth grade",
                "abbr" => "10",
                "short_description" => "Congruent triangles, geometric constructions, colons, word patterns, audience and tone, and more."
            ],
            [
                "name" => "Eleventh grade",
                "abbr" => "11",
                "short_description" => "Trigonometric functions, logarithms, polynomials, supporting evidence, claims and counterclaims, and more."
            ],
            [
                "name" => "Twelfth grade",
                "abbr" => "12",
                "short_description" => "Complex numbers, derivatives, limits, domain-specific vocabulary, writing arguments, and more."
            ]
        ];

        $mathGrades = [
            [
                "name" => "Pre-K",
                "code" => "P"
            ],
            [
                "name" => "Kindergarten",
                "code" => "K"
            ],
            [
                "name" => "First grade",
                "code" => "1"
            ],
            [
                "name" => "Second grade",
                "code" => "2"
            ],
            [
                "name" => "Third grade",
                "code" => "3"
            ],
            [
                "name" => "Fourth grade",
                "code" => "4"
            ],
            [
                "name" => "Fifth grade",
                "code" => "5"
            ],
            [
                "name" => "Sixth grade",
                "code" => "6"
            ],
            [
                "name" => "Seventh grade",
                "code" => "7"
            ],
            [
                "name" => "Eighth grade",
                "code" => "8"
            ],
            [
                "name" => "Algebra 1",
                "code" => "A1"
            ],
            [
                "name" => "Geometry",
                "code" => "G"
            ],
            [
                "name" => "Algebra 2",
                "code" => "A2"
            ],
            [
                "name" => "Precalculus",
                "code" => "PC"
            ],
            [
                "name" => "Calculus",
                "code" => "C"
            ]
        ];

        $languageArtsGrades = [
            [
                "name" => "Pre-K",
                "code" => "P"
            ],
            [
                "name" => "Kindergarten",
                "code" => "K"
            ],
            [
                "name" => "First grade",
                "code" => "1"
            ],
            [
                "name" => "Second grade",
                "code" => "2"
            ],
            [
                "name" => "Third grade",
                "code" => "3"
            ],
            [
                "name" => "Fourth grade",
                "code" => "4"
            ],
            [
                "name" => "Fifth grade",
                "code" => "5"
            ],
            [
                "name" => "Sixth grade",
                "code" => "6"
            ],
            [
                "name" => "Seventh grade",
                "code" => "7"
            ],
            [
                "name" => "Eighth grade",
                "code" => "8"
            ],
            [
                "name" => "Ninth grade",
                "code" => "9"
            ],
            [
                "name" => "Tenth grade",
                "code" => "10"
            ],
            [
                "name" => "Eleventh grade",
                "code" => "11"
            ],
            [
                "name" => "Twelfth grade",
                "code" => "12"
            ]
        ];

        $scienceGrades = [
            [
                "name" => "Second grade",
                "code" => "2"
            ],
            [
                "name" => "Third grade",
                "code" => "3"
            ],
            [
                "name" => "Fourth grade",
                "code" => "4"
            ],
            [
                "name" => "Fifth grade",
                "code" => "5"
            ],
            [
                "name" => "Sixth grade",
                "code" => "6"
            ],
            [
                "name" => "Seventh grade",
                "code" => "7"
            ],
            [
                "name" => "Eighth grade",
                "code" => "8"
            ]
        ];

        $socialStudiesGrades =  [
            [
                "name" => "Second grade",
                "code" => "2"
            ],
            [
                "name" => "Third grade",
                "code" => "3"
            ],
            [
                "name" => "Fourth grade",
                "code" => "4"
            ],
            [
                "name" => "Fifth grade",
                "code" => "5"
            ],
            [
                "name" => "Sixth grade",
                "code" => "6"
            ],
            [
                "name" => "Seventh grade",
                "code" => "7"
            ],
            [
                "name" => "Eighth grade",
                "code" => "8"
            ]
        ];

        $this->creatingGrades($mathGrades, Subject::MATH);
        $this->creatingGrades($languageArtsGrades, Subject::LANGUAGE_ARTS);
        $this->creatingGrades($scienceGrades, Subject::SCIENCE);
        $this->creatingGrades($socialStudiesGrades, Subject::SOCIAL_STUDIES);
    }

    public function creatingGrades($grades, $subjectName)
    {
        $subject = Subject::where('name', $subjectName)->firstOrFail();

        foreach ($grades as $grade) {
            $foundedGrade = collect($this->allGradesWithShortDescription)->firstWhere('name', $grade['name']);
            $shortDescription = $foundedGrade['short_description'] ?? null;

            $gradeModel = Grade::firstOrCreate(
                array_merge($grade, [
                    'short_description' => $shortDescription
                ])
            );

            $subject->grades()->attach($gradeModel->id);
        }
    }
}
