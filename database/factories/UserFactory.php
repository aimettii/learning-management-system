<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => Hash::make('password'), // password
            'remember_token' => Str::random(10),
        ];
    }

    public function role($role)
    {
        return $this->state(function (array $attributes) {
            return $attributes;
        })->afterCreating(function (User $user) use($role) {
            if (! in_array($role, Role::getAllRoles())) {
                throw new \Exception('Not a possible role `' . $role . '` was passed');
            }

            $user->syncRoles($role);
        });
    }

    public function configure()
    {
        return $this->afterMaking(function (User $user) {

        })->afterCreating(function (User $user) {

        });
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
