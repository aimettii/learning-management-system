<?php

namespace Database\Factories;

use App\Models\Question;
use App\Models\Skill;
use App\Models\Subject;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class SkillFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Skill::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
//        $subject = Subject::inRandomOrder()->first();
//        $grade = $subject->grades()->inRandomOrder()->first();
//        $topic = $subject->topics()->inRandomOrder()->first();

        return [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'shortcut' => Skill::generateShortcut(),
//            'category_title' => $topic->name . ' ' . $this->faker->jobTitle,
//            'grade_id' => $grade->id,
//            'topic_id' => $topic->id,
        ];
    }

    public function topic($name)
    {
        return $this->state(function (array $attributes) use ($name) {
            return array_merge($attributes, [
                'category_title' =>  $name . ' ' . $this->faker->jobTitle
            ]);
        });
    }

    public function configure()
    {
        return $this->afterCreating(function (Skill $skill) {
            Question::factory(5)->create([
                'skill_id' => $skill->id,
            ]);
        });
    }
}
