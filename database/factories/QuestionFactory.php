<?php

namespace Database\Factories;

use App\Models\Question;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Question::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
//        $skill = Skill::inRandomOrder()->first();
        return [
            'title' => $this->faker->jobTitle,
//            'skill_id' => $skill->id,
            'correct_answer' => json_encode([]),
            'rendering_question' => json_encode([]),
        ];
    }
}
