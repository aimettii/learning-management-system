<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProfileAttributesCreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableRoles = config('permission.table_names.roles');

        Schema::create('profile_attributes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('role_profile_attributes', function (Blueprint $table) use($tableRoles) {
            $table->id();
            $table->foreignId('role_id')->constrained($tableRoles)->onDelete('cascade');
            $table->foreignId('profile_attribute_id')->constrained('profile_attributes')->onDelete('cascade');
            $table->boolean('is_required');
        });

        Schema::create('user_profile_attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('profile_attribute_id')->constrained('profile_attributes')->onDelete('cascade');
            $table->string('profile_attribute_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_attributes');
        Schema::dropIfExists('role_profile_attributes');
        Schema::dropIfExists('user_profile_attributes');
    }
}
