<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('guest')->group(function () {
    Route::resource('/skills', 'SkillsAPIController');
    Route::get('/subjects/{subject}/grades', 'GradesAPIController@indexBySubject');
    Route::get('/subjects/{subject}/topics', 'TopicsAPIController@indexBySubject');
    Route::get('/topics/{topicSlug}/categories', 'TopicsAPIController@indexCategories');
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/auth/account', 'AuthController@account');
    Route::get('/billing/subscription', 'BillingAPIController@getSubscriptionDetails');
    Route::post('/billing/upgrade', 'BillingAPIController@upgrade');
    Route::get('/billing/setup-intent', 'BillingAPIController@getIntent');

    Route::namespace('admin')->middleware(['role:admin'])->group(function () {
        Route::resource('/skills', 'SkillsAPIController');
        Route::get('/subjects/{subject}/grades', 'GradesAPIController@indexBySubject');
        Route::get('/subjects/{subject}/topics', 'TopicsAPIController@indexBySubject');
        Route::get('/topics/{topicSlug}/categories', 'TopicsAPIController@indexCategories');
    });
});


