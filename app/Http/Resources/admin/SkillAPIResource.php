<?php

namespace App\Http\Resources\admin;

use Illuminate\Http\Resources\Json\JsonResource;

class SkillAPIResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request) + [
                'questions' => $this->questions,
                'topic' => $this->topic,
                'grade' => $this->grade,
                'subject' => $this->subject,
            ];
    }
}
