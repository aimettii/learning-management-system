<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ConvertStringBooleans
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->replace($this->transform($request->all()));
        return $next($request);
    }

    /**
     * Transform boolean strings to boolean
     * @param array $parameters
     * @return array
     */
    private function transform(array $parameters): array
    {
        function mapFunc($param) {
            if (is_string($param)) {
                if ($param == 'true' || $param == 'false') {
                    return filter_var($param, FILTER_VALIDATE_BOOLEAN);
                }

                return $param;
            } else if (is_array($param)) {
                foreach ($param as $k => $p) {
                    $param[$k] = mapFunc($p);
                }

                return $param;
            };
        };

        return collect($parameters)->map(function ($item) {
            return mapFunc($item);
        })->all();
    }
}