<?php

namespace App\Http\Controllers\API\admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\admin\SkillAPIResource;
use App\Models\File;
use App\Models\Skill;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SkillsAPIController extends Controller
{
    public function index(Request $request)
    {
        $query = QueryBuilder::for(Skill::class)
            ->allowedFilters([
                AllowedFilter::callback('grade', function ($query, $value) {
                    $query->whereHas('grade', function ($query) use ($value) {
                        if ($value !== '*') {
                            $query->where('code', $value);
                        }
                    });
                }),
                AllowedFilter::callback('topic', function ($query, $value) {
                    $query->whereHas('topic', function ($query) use ($value) {
                        if ($value !== '*') {
                            $query->where('slug', $value);
                        }
                    });
                }),
                AllowedFilter::callback('search', function ($query, $value) {
//                            $query->search($value);
                }),
                AllowedFilter::callback('subject', function ($query, $value) {
                    $query->whereHas('topic', function ($query) use ($value) {
                        $query->whereHas('subject', function ($query) use ($value) {
                            $query->where('name', $value);
                        });
                    });
                }),
            ])
            ->allowedSorts('created_at');

        if ($request->filter['search'] ?? false) {
//            $query = Skill::search($request->filter['search'])->constrain($query);
            $query = $query->where('title', 'like', '%' . $request->filter['search'] . '%')
                ->orWhere('shortcut', 'like', '%' . $request->filter['search'] . '%')
                ->orWhere('category_title', 'like', '%' . $request->filter['search'] . '%')
                ->orWhere('created_at', 'like', '%' . $request->filter['search'] . '%');
        }

        return $this->sendResponse(
            SkillAPIResource::collection(
                $query
                    ->paginate($request->limit)
                    ->appends(request()->query())
            ), 'Skills retrieved successfully');
    }

    public function show($param)
    {
        if (is_int($param)) {
            $skill = Skill::findOrFail($param)->firstOrFail();
        } else {
            $skill = Skill::shortcut($param)->firstOrFail();
        }

        return $this->sendResponse(new SkillAPIResource($skill), 'Skills retrieved successfully');
    }

    public function store(Request $request)
    {
        $file = File::upload($request->file('image'));

        return $file;
    }
}
