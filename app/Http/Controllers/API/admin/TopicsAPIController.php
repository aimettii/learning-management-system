<?php

namespace App\Http\Controllers\API\admin;

use App\Http\Controllers\Controller;
use App\Models\Skill;
use App\Models\Subject;
use App\Models\Topic;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TopicsAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = QueryBuilder::for(Topic::class)
            ->allowedFilters([
                AllowedFilter::callback('search', function ($query, $value) {
//                            $query->search($value);
                }),
            ])
            ->allowedSorts('created_at');

        if ($request->filter['search'] ?? false) {
            $query = Topic::search($request->filter['search'])->constrain($query);
        }

        return $this->sendResponse($query
            ->paginate($request->limit)
            ->appends(request()->query()), 'Topics retrieved successfully');
    }

    public function indexBySubject($subject, Request $request)
    {
        $subject = Subject::where('name', $subject)->firstOrFail();

        $query = QueryBuilder::for(Topic::class)
            ->allowedFilters([
                AllowedFilter::callback('search', function ($query, $value) {
//                            $query->search($value);
                }),
            ])
            ->allowedSorts('created_at');

        $query->where('subject_id', $subject->id);

        if ($request->filter['search'] ?? false) {
//            $query = Topic::search($request->filter['search'])->constrain($query);
           $query = $query->where('name', 'like', '%' . $request->filter['search'] . '%');
        }

        return $this->sendResponse($query
            ->paginate($request->limit)
            ->appends(request()->query()), 'Topics retrieved successfully');
    }

    public function indexCategories($topicSlug, Request $request)
    {
        $query = QueryBuilder::for(Skill::class)
            ->allowedFilters([
                AllowedFilter::callback('search', function ($query, $value) {
//                            $query->search($value);
                }),
            ])
            ->allowedSorts('created_at');

        $query = $query->whereHas('topic', function ($query) use ($topicSlug) {
            $query->where('slug', $topicSlug);
        });

        if ($request->filter['search'] ?? false) {
//            $query = Skill::search($request->filter['search'])->constrain($query);
            $query = $query->where('category_title', 'like', '%' . $request->filter['search'] . '%');
        }

        return $this->sendResponse($query
            ->paginate($request->limit)
            ->appends(request()->query()), 'Topic categories retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
