<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccountResource;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthenticationException
     */
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string']
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw new AuthenticationException();
        }

        if ($request->_through_token) {
            return $this->sendResponse(['token' => $user->createToken($request->email)->plainTextToken], 'Authorization successful, API token was issued');
        } else {
            if (Auth::attempt($credentials, $request->remember_me)) {
                return $this->sendSuccess('Authorization successful, Cookies have been set');
            } else {
                throw new AuthenticationException();
            }
        }
    }

    public function account(Request $request)
    {
        return $this->sendResponse(new AccountResource($request->user()), 'User account retrieved successfully');
    }

    public function logout()
    {
        Auth::logout();

        return $this->sendSuccess('Cleaning the authentication information was successful');
    }
}
