<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Http\Request;

class BillingAPIController extends Controller
{
    public function upgrade(Request $request)
    {
        $request->validate([
            'payment_method_id' => 'required',
            'subscribe' => 'required|string'
        ]);
        
        $subscriptionAttempt = $request->user()->upgradeToPlan($request->subscribe, $request->payment_method_id);



        if ($subscriptionAttempt === Subscription::SUBSCRIBE_FAIL_ALREADY_SUBSCRIBED && $request->user()->subscription($request->subscribe)) {
            $subscribe = $request->user()->subscription($request->subscribe);
            $subscribe['pm_type'] = $request->user()->fresh()->pm_type;
            $subscribe['pm_last_four'] = $request->user()->fresh()->pm_last_four;

            return $this->sendResponse($request->user()->subscription($request->subscribe), 'Already subscribed');
        }

        $subscriptionAttempt = $subscriptionAttempt->toArray();

        $subscriptionAttempt['pm_type'] = $request->user()->fresh()->pm_type;
        $subscriptionAttempt['pm_last_four'] = $request->user()->fresh()->pm_last_four;

        if ($subscriptionAttempt instanceof \Laravel\Cashier\Subscription) {
            return $this->sendResponse($subscriptionAttempt, 'Success upgraded to plan');
        } else {
            return $this->sendError('An internal error occurred while upgrading', 500, null,
                is_string($subscriptionAttempt) ? $subscriptionAttempt : null);
        }
    }

    public function getSubscriptionDetails()
    {
        $subscription = auth('sanctum')->user()->getSubscription();

        if (is_null($subscription)) {
            return $this->sendError('Subscription not found', 404);
        }

        $subscription = $subscription->toArray();

        return $this->sendResponse($subscription, 'Subscription retrieved successfully');
    }

    public function addPaymentMethod(Request $request)
    {
        $request->validate([
            'payment_method_id' => 'required'
        ]);

        $paymentMethod = $request->user()->findPaymentMethod($request->payment_method_id);

        if ($paymentMethod) {
            return $this->sendError('Payment method already added');
        }

        return $this->sendResponse($request->user()->addPaymentMethod($request->payment_method_id), 'Payment method added successfully');
    }

    public function getIntent(Request $request)
    {
        return $this->sendResponse($request->user()->createSetupIntent(), 'Setup intent');
    }

}
