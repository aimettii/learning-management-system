<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
    public function show($uniqueId)
    {
        $file = File::where('unique_id', $uniqueId)->firstOrFail();

        if (!Storage::exists($file->path)) {
            return $this->sendError('File not found');
        }

        return response()->file(Storage::path($file->path));
    }
}
