<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const API_VERSION = '1.0';

    public function sendResponse($result, $message)
    {
        $arrResponse = self::makeResponse($message, $result);

        if ($result instanceof ResourceCollection && $result->resource instanceof LengthAwarePaginator) {
            $arrResponse = array_merge($arrResponse, (array) $result->toResponse(new Request())->getData());
        }

        if ($result instanceof LengthAwarePaginator) {
            $paginatorArr = $result->toArray();
            $arrResponse['data'] = $paginatorArr['data'];
            unset($paginatorArr['data']);
            $arrResponse = array_merge($arrResponse, ['meta' => $paginatorArr]);
        }

        return Response::json($arrResponse);
    }

    public function sendError($error, $code = 404)
    {
        $arrResponse = self::makeError($error);

        return Response::json($arrResponse, $code);
    }

    public function sendSuccess($message, $code = 200)
    {
        return Response::json([
            'success' => true,
            'message' => $message,
            '_reference' => self::API_VERSION
        ], $code);
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
    public static function makeError($message, array $data = [])
    {
        $res = [
            'success' => false,
            'message' => $message,
            '_reference' => self::API_VERSION,
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        return $res;
    }

    /**
     * @param string $message
     * @param mixed  $data
     *
     * @return array
     */
    public static function makeResponse($message, $data)
    {
        return [
            'success' => true,
            'data'    => $data,
            'message' => $message,
            '_reference' => self::API_VERSION
        ];
    }
}
