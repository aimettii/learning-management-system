<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class File extends Model
{
    use HasFactory;

    protected $table = 'files';

    protected $guarded = [];

    public static function upload(UploadedFile $file, $path = '/')
    {
        $uniqueId = uniqid();
        $filename = $uniqueId . '.' . $file->extension();
        $path = $file->storeAs(
            $path, $filename
        );

        $file = File::create([
            'unique_id' => $uniqueId,
            'disk' => config('filesystems.default'),
            'path' => $path,
            'extension' => $file->extension(),
            'original_name' => $file->getClientOriginalName(),
            'mime_type' => $file->getClientMimeType(),
            'size' => $file->getSize(),
        ]);

        return $file;
    }
}
