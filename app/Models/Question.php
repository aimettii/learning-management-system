<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $casts = [
        'rendering_question' => 'array',
        'correct_answer' => 'array',
    ];

    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }
}
