<?php

namespace App\Models;

use App\Events\UserUpgradedPricePlan;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = ['role'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRoleAttribute()
    {
        return $this->roles->first();
    }

    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class);
    }

    public function questionsAnswered()
    {
        return $this->belongsToMany(Question::class, 'question_answers', 'user_id', 'question_id');
    }

    /**
     * Does the user have an associated payment method
     *
     * @return bool
     */
    public function getHasPaymentMethodAttribute()
    {
        return $this->hasPaymentMethod();
    }

    public function getSubscription()
    {
        return $this->subscriptions()->active()->first();
    }

    public function updatePaymentMethod($paymentMethodId)
    {
        return $this->addPaymentMethod($paymentMethodId);
    }

    public function upgradeToPlan($plan, $paymentMethod = null)
    {
        if (!$this->subscribed($plan)) {
//            $paymentMethod = $this->updatePaymentMethod($paymentMethod);

            $subscription = null;

            switch ($plan) {
                case Subscription::SUBSCRIBE_TEACHER:
                    $planId = Subscription::TEACHER_PLAN_ID;
                    break;
                case Subscription::SUBSCRIBE_PARENT:
                    $planId = Subscription::PARENT_PLAN_ID;
                    break;
                case Subscription::SUBSCRIBE_PRO:
                    $planId = Subscription::PRO_PLAN_ID;
                    break;
                default:
                    throw new \Exception('Bad plan');
            }

            try {
                $subscription = $this->newSubscription($plan, $planId)
                    ->create($paymentMethod);
            } catch (\Exception $exception) {
                return Subscription::SUBSCRIBE_FAIL;
            }

            return $subscription;
        } else {
            return Subscription::SUBSCRIBE_FAIL_ALREADY_SUBSCRIBED;
        }
    }
}
