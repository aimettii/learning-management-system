<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    const MATH = 'math';
    const LANGUAGE_ARTS = 'language_arts';
    const SCIENCE = 'science';
    const SOCIAL_STUDIES = 'social_studies';

    public static function getAllSubjects()
    {
        return [self::MATH, self::LANGUAGE_ARTS, self::SCIENCE, self::SOCIAL_STUDIES];
    }

    public function grades()
    {
        return $this->belongsToMany(Grade::class, 'grade_subject', 'subject_id', 'grade_id');
    }

    public function topics()
    {
        return $this->hasMany(Topic::class);
    }
}