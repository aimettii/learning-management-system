<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class Topic extends Model
{
    use Searchable;
    use Sluggable;
    use HasFactory;

    protected $fillable = [
        'title'
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['name'] = utf8_encode((new TNTIndexer)->buildTrigrams($this->name));

        return $array;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function skills()
    {
        return $this->hasMany(Skill::class);
    }
}
