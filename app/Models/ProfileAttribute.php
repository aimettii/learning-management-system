<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileAttribute extends Model
{
    protected $table = 'profile_attributes';

    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const AVATAR = 'avatar';

    public $timestamps = false;

    public static function getAllAttributes()
    {
        return [self::FIRST_NAME, self::LAST_NAME, self::AVATAR];
    }
}
