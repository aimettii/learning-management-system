<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends \Spatie\Permission\Models\Role
{
    const ADMIN = 'admin';
    const TEACHER = 'teacher';
    const STUDENT = 'student';
    const PARENT = 'parent';

    public static function getAllRoles()
    {
        return [self::ADMIN, self::TEACHER, self::STUDENT, self::PARENT];
    }

    public function profileAttributes()
    {
        return $this->belongsToMany(ProfileAttribute::class, 'role_profile_attributes', 'role_id', 'profile_attribute_id')
            ->withPivot('is_required');
    }
}
