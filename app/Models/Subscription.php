<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends \Laravel\Cashier\Subscription
{
    const SUBSCRIBE_PARENT = 'parent';
    const SUBSCRIBE_TEACHER = 'teacher';
    const SUBSCRIBE_PRO = 'pro';
    const SUBSCRIBE_FAIL_ALREADY_SUBSCRIBED = 'subscribe_fail_already_subscribed';
    const SUBSCRIBE_FAIL = 'subscribe_fail';
    const SUBSCRIBE_FAIL_BAD_PAYMENT_METHOD = 'subscribe_fail_bad_payment_method';
    const SUBSCRIBE_FAIL_NOT_FOUND_PAYMENT_METHOD = 'subscribe_fail_not_found_payment_method';
    const SUBSCRIBE_SUCCESS = 'subscribe_success';

    const PARENT_PLAN_ID = 'price_1JoT5QCyQcbN3D32OJb3AYXp';
    const TEACHER_PLAN_ID = 'price_1JoT5uCyQcbN3D32tHFO8xsP';
    const PRO_PLAN_ID = 'price_1JoT6BCyQcbN3D32X6QOe2rL';
}
