<?php

namespace App\Models;

use Faker\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;
use Rorecek\AutoIncrement\Facades\AutoIncrement;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class Skill extends Model
{
    use Searchable;
    use HasFactory;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['title'] = utf8_encode((new TNTIndexer)->buildTrigrams($this->title));
        $array['shortcut'] = utf8_encode((new TNTIndexer)->buildTrigrams($this->shortcut));
        $array['category_title'] = utf8_encode((new TNTIndexer)->buildTrigrams($this->category_title));

        return $array;
    }

    public function scopeShortcut($query, $shortcut)
    {
        return $query->where('shortcut', $shortcut);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function subject()
    {
        return $this->topic->subject();
    }

    public static function generateShortcut($id =null)
    {
        $maxCombinationsCount = function ($template) {
            $arr = str_split($template);

            $count = 1;

            foreach ($arr as $i) {
                switch ($i) {
                    case '?':
                        $count *= 26;
                        break;
                    case '#':
                        $count *= 9;
                        break;
                }
            }

            return $count;
        };

        $shortcutTemplate = '?#?';

        if (!$id) {
            $autoIncrement = DB::table('skills')->max('id') + 1;
            AutoIncrement::table('skills')->set($autoIncrement + 1);
        } else {
            $autoIncrement = $id;
        }

        for($i = 0; $i < 100; $i++) {
            $maxCount = $maxCombinationsCount($shortcutTemplate);
            if (
                $autoIncrement > $maxCount && strlen($shortcutTemplate) <= 3  ||
                strlen($shortcutTemplate) > 3 && $autoIncrement - $maxCombinationsCount(substr($shortcutTemplate, 0, -1)) > $maxCount
            ) {

                $shortcutTemplate .= strlen($shortcutTemplate) % 2 == 0 ? '?' : '#';
                continue;
            } else {
                break;
            }
        }

        $englishLetters = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        );

        $numbers = array(
            1, 2, 3, 4, 5, 6, 7, 8, 9,
        );

        $templateGroups = str_split($shortcutTemplate);
        $generatedShortcut = '';

        $combinationsGroupCount = 1;

        foreach ($templateGroups as $key => $templateGroup) {
            switch ($templateGroup) {
                case '?':
                    $symbolsArr = $englishLetters;
                    $combinationsGroup = 26;
                    break;
                case '#':
                    $symbolsArr = $numbers;
                    $combinationsGroup = 9;
                    break;
            }

            if ($key + 1 > 3) {
                $autoIncrement -= $combinationsGroupCount;
            }

            $generatedShortcut .=  array_slice($symbolsArr,  (int) ceil($autoIncrement / $combinationsGroupCount) % $combinationsGroup - 1, 1)[0];
            $combinationsGroupCount *= $combinationsGroup;
        }

        return $generatedShortcut;
}
}
