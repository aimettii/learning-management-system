<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return response()->json([
                'message' => 'You do not have the required authorization.',
                'success' => false,
                '_reference' => Controller::API_VERSION
            ], 403);
        } elseif ($exception instanceof AuthenticationException) {
            return response()->json([
                'message' => $exception->getMessage(),
                'success' => false,
                '_reference' => Controller::API_VERSION
            ], 401);
        } elseif ($exception instanceof ValidationException) {
            return response()->json([
                'message' => $exception->getMessage(),
                'errors' => $exception->errors(),
                'validationError' => true,
                'success' => false,
                '_reference' => Controller::API_VERSION
            ], $exception->status);
        }

        $response = parent::render($request, $exception);

        if ($response instanceof \Symfony\Component\HttpFoundation\Response) {
            $message = App::environment('local') ? $exception->getMessage() : null;

            if (is_null($message) || empty($message)) {
                $message = $response->exception instanceof NotFoundHttpException ? 'Not found http exception' : null;
            }

            return response()->json(collect([
                'message' => $message,
                'success' => false,
                '_unhandled' => true,
                '_reference' => Controller::API_VERSION
            ])
                ->filter(function ($item) {return is_bool($item) || !empty($item);})
                ->all()
                , $response->getStatusCode() ?? 500);
        }

        return $response;
    }
}
